import { Router } from 'express';
import appointmentsRouter from './appointments.routes';

const routes = Router();

//qdo o frontend chamar o http://localhost:3333/appointments 

routes.use('/appointments', appointmentsRouter);

export default routes;