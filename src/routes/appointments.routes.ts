import { response, Router } from 'express';
import { v4 as uuid} from 'uuid';
import { startOfHour, parseISO, isEqual } from 'date-fns';

const appointmentsRouter = Router();

interface Appointment {
    id: string;
    provider: string;
    date: Date;
}

const appointments: Appointment[] = [];

appointmentsRouter.post('/', (request, response) => {
    const { provider, date } = request.body;

    const parsedDate  = startOfHour(parseISO(date));
    const findAppointmentInTheSameDate = appointments.find(appointment  => isEqual(parsedDate, appointment.date),);

    if (findAppointmentInTheSameDate) {
        return response
             .status(400)
             .json({ message: 'This appointment is already booking '});
    }

    const appointment = {
        id: uuid(),
        provider, 
        date, 
    };
    
    appointments.push(appointment);
    

});


export default appointmentsRouter;